# Hapi Demo#

This is a nodejs hapi application. Its a demo app to try out things before actually trying them in other apps. 

### You can use this to test new authentication strategy, new plugins, experimental libraries and so on ###

### How do I get set up? ###

* Clone repo
* npm install
* npm start
* goto localhost:8000

### Who do I talk to? ###

* Kunle kunle@kooltank.com