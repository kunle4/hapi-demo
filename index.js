
const Hapi = require('hapi');
 
// Create a server with a host and port
const server = new Hapi.Server();
server.connection({ 
    host: 'localhost', 
    port: 8000 
});

server.ext("onRequest",function(request, reply){
    console.log("Request received: " + request.path);
    reply.continue();
});

var googleAuthOptions = {
    REDIRECT_URL: '/auth/google/callback', 
    handler: require('./server/user/routes').google, 
    scope: 'https://www.googleapis.com/auth/plus.profile.emails.read'
};

var linkedinAuthOptions = {
    REDIRECT_URL: '/auth/linkedin/callback', 
    handler: require('./server/user/routes').linkedIn, 
    //scope: 'https://www.googleapis.com/auth/plus.profile.emails.read'
};

//register plugins
var plugins = [{register:require('inert')}, 
                {register:require('vision')},
                { register: require('hapi-auth-google'), options : googleAuthOptions },
                { register: require('./server/user/hapi-auth-linkedin'), options : linkedinAuthOptions }
               ];

server.register(plugins, (err) => {
    if (err) {
        throw err;
    }
});

server.views({
    engines: {
        html: require('handlebars')
    },
    relativeTo: __dirname,
    path: 'templates'
});

//extensions
server.decorate('request', 'generate_google_oauth2_url', server.generate_google_oauth2_url);
server.decorate('request', 'generateOauthUrl', server.generateOauthUrl);

// Add the route
server.route({
    method: 'GET',
    path: '/public/{path*}',
    handler: {
        directory:{
            path:'public'
        }
    }
});

server.route(require('./server/user/routes').routes);

// Start the server
server.start((err) => {
    if (err) {
        throw err;
    }
    console.log('Server running at:', server.info.uri);
});