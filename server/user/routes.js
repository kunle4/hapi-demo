var userController = require('./user-controller');
routes = [
    {
        method: 'GET',
        path: '/user/{id}',
        handler: userController.getUser
    },
    {
        method: 'POST',
        path: '/user',
        handler: userController.createUser
    },
    {
        method: 'GET',
        path: '/google',
        handler: (request, reply) =>{
            var url = request.generate_google_oauth2_url();
                var imgsrc = 'https://developers.google.com/accounts/images/sign-in-with-google.png';
                var btn = `<a href="${url}"><img src="${imgsrc}" style="width: 18%;" alt="Login With Google"></a>`
            reply(btn);
        }
    },
    {
        method: 'GET',
        path: '/linkedin',
        handler: (request, reply) =>{
            var url = request.generateOauthUrl();
                var imgsrc = 'https://content.linkedin.com/content/dam/developer/global/en_US/site/img/signin-button.png';
                var btn = `<a href="${url}"><img src="${imgsrc}" alt="Login With LinkedIn"></a>`
            reply(btn);
        }
    }
];

var google = function(request, reply, tokens, profile){
    console.log(profile);
    if(profile) {
        // extract the relevant data from Profile to store in JWT object
        var user = {
        firstname : profile.name.givenName, // the person's first name e.g: Anita
        braggingRights: profile.braggingRights,
        userImage    : profile.image.url.replace('?sz=50',''),      // profile image url
        id       : profile.id,             // google+ id
        exp      : Math.floor(new Date().getTime()/1000) + 7*24*60*60, // Epiry in seconds!
        agent    : request.headers['user-agent'],
        location : profile.placesLived ? profile.placesLived[profile.placesLived.length - 1].value: 'no location',
        email: profile.emails[0].value,
        occupation: profile.occupation
        }
        // create a JWT to set as the cookie:
        //var token = JWT.sign(session, process.env.JWT_SECRET);
        // store the Profile and Oauth tokens in the Redis DB using G+ id as key
        // Detailed Example...? https://github.com/dwyl/hapi-auth-google/issues/2

        // reply to client with a view
        return reply.view('user-Profile',user);
        //.state('token', token); // see: http://hapijs.com/tutorials/cookies
    }
    else {
        return reply.redirect('/google');
    }
};

var linkedIn = function(request, reply, tokens, profile){
    console.log(profile);
    if(profile) {
        // extract the relevant data from Profile to store in JWT object
        var user = {
        firstname : profile.firstName,
        lastname:profile.lastName, // the person's first name e.g: Anita
        userImage    :profile.pictureUrl,      // profile image url
        id       : profile.id,             // google+ id
        agent    : request.headers['user-agent'],
        email: profile.emailAddress?profile.emailAddress : null,
        braggingRights: profile.headline,
        location: (profile.location && profile.location.name) ? profile.location.name: null
        };
        
        console.log('-------------------------create user');
        console.log(user);
        return reply.view('user-Profile',user);
        //.state('token', token); // see: http://hapijs.com/tutorials/cookies
    }
    else {
        return reply.redirect('/linkedin');
    }
};

exports.routes = routes;
exports.google = google;
exports.linkedIn = linkedIn;