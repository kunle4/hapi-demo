'use strict';

var userStore = require('./user-store');
var Boom = require('boom');


var createUser = function(request, reply){
    var user = {
        username:request.payload.username,
        email:request.payload.email
    };

    userStore.createUser(user, function(err){
        if(err){
            Boom.conflict(err.message);
            reply();
        }
        else{
            reply.redirect('/user-good.html');
        }
    });    
};

var getUser = function(request, reply){
    userStore.getUser(request.params.id, function(err, user){
    if(err){
            Boom.create(500,'Unable to get user, please try again');
        }
        else{
           reply.view('user',user);
        }
    });
};

exports.createUser = createUser;
exports.getUser = getUser;