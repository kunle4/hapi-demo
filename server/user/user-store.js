'use strict';

var userStore = {};
var Boom = require('boom');

var createUser = function(user, callback){
    if(userStore[user.email]){
        let error = new Error('User already exist!, please try to login with your email');
        callback(error);
    }
    else{
        userStore[user.email] = user;
        callback();
    }
}

var getUser = function(id, callback){
    callback(null, userStore[id]);
}

exports.createUser = createUser;
exports.getUser = getUser;