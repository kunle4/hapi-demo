var request = require('request');
var OPTIONS;     // global object set when plugin loads
const LINKEDIN_OAUTH_BASE_URL = 'https://www.linkedin.com/oauth/v2/authorization';
const LINKEDIN_TOKEN_BASE_URL = 'https://www.linkedin.com/oauth/v2/accessToken';
const LINKEDIN_PEOPLE_API_URL = 'https://www.linkedin.com/v1/people';

const RESPONSE_TYPE_PARAM = 'response_type';
const CLIENT_ID_PARAM = 'client_id';
const REDIRECT_URI_PARAM = 'redirect_uri';
const SCOPE_PARAM = 'scope';
const STATE_PARAM = 'state';


const GRANT_TYPE_PARAM = 'grant_type';
const CODE_PARAM = 'code';
const CLIENT_SECRET_PARAM = 'client_secret';

var oauth2Option = {
    clientId : process.env.LINKEDIN_CLIENT_ID,
    clientSecret : process.env.LINKEDIN_CLIENT_SECRET
    //redirectUri : OPTIONS.REDIRECT_URL
}
//https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=123456789&redirect_uri=https%3A%2F%2Fwww.example.com%2Fauth%2Flinkedin&state=987654321&scope=r_basicprofile
/**
 * create_oauth2_client creates the OAuth2 client
 *  @param {Object} options - the options passed into the plugin
 *  @returns {Object} oauth2_client - the Google OAuth2 client
 */
function create_oauth2_client () {
  oauth2_client = new OAuth2Client(
    process.env.GOOGLE_CLIENT_ID,
    process.env.GOOGLE_CLIENT_SECRET,
    process.env.BASE_URL + OPTIONS.REDIRECT_URL
  );
  return oauth2_client;
}

var populateRedirectUrI = function(redirectUri){
  oauth2Option.redirectUri = 'http://127.0.0.1:8000' + redirectUri;
};


/**
 *  generateOauthUrl creates a url where the user is sent to authenticate
 *  no param
 *  @returns {String} url - the url where people visit to authenticate
 */
var generateOauthUrl = function() {
  return `${LINKEDIN_OAUTH_BASE_URL}?${RESPONSE_TYPE_PARAM}=code&${CLIENT_ID_PARAM}=${oauth2Option.clientId}&${REDIRECT_URI_PARAM}=${oauth2Option.redirectUri}&${SCOPE_PARAM}=r_basicprofile&${STATE_PARAM}=Hkdfst13290c0llcm7cnV`;
}

/**
 * getAccessToken gets a token that is used for authrozation
 * param:code The code recieved during authentication
 * param:redirectUri The callback the auth provider needs to call, same as what is set on the provider
 * param:callback The callback that should be called when response comes back
 * @returns no return
 */
var getAccessToken = function(code,callback){
    var options = {
      method: 'POST',
      url: LINKEDIN_TOKEN_BASE_URL,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      form :{
          grant_type : 'authorization_code',
          code : code,
          client_secret: oauth2Option.clientSecret,
          client_id: oauth2Option.clientId,
          redirect_uri: oauth2Option.redirectUri
        }
    };

    request(options,function(err,httpResponse,body){
        if(err) {
          console.log(err);
          console.log(httpResponse);
        };
       // console.log(httpResponse);
       var bodyObject = JSON.parse(body);
       //console.log(`body object is${typeof bodyObject}`);
       //console.log(bodyObject);
        
        var accessToken = bodyObject.access_token;
        callback(err,accessToken);
    });
};

/**
 * getUserProfile Gets the user profile
 * param: accessToken the access token recieved after authorization
 * param: callback the callback that will be called when response is comes back
 * @returns no return
 */
var getUserProfile = function (accessToken, callback) {
  var fields = ':(id,first-name,last-name,email-address,num-connections,picture-url,location,headline)';
  var options = {
    url: `${LINKEDIN_PEOPLE_API_URL}/~${fields}?format=json&oauth2_access_token=${accessToken}`/*,
    headers: {
      'Authorization': accessToken
    }*/
  };
  request(options, function(err, response, body){
    if(err){
      console.log(response);
      Console.log(err);
      callback(err,body);
    }
    callback(err,body);
  });
};

exports.register = function (server, options, next) {
  populateRedirectUrI(options.REDIRECT_URL);
  OPTIONS = options;
  //oauth2_client = create_oauth2_client(options);
  server.decorate('server', 'generateOauthUrl', generateOauthUrl)
  server.route([
    {
      method: 'GET',
      path: options.REDIRECT_URL, // must be identical to Authorized redirect URI
      config: {auth: false},
      handler: function (req, reply) {
        var code = req.query.code;
        console.log(' - - - - - - - - - - - - code:');
        console.log(code);
        getAccessToken(code, function(err, tokens) {
            // anita's error handler
            if(err){
              return options.handler(req, reply, tokens, null);
            }

            getUserProfile(tokens, function(err,profile){
              profile = JSON.parse(profile);
              return options.handler(req,reply,tokens,profile);
            });          
        });
      }
    }
  ]);

  next(); // everything worked, continue booting the hapi server!
};

exports.register.attributes = {
    pkg: {
      name:'hapi-auth-linkedin',
      version:'0.0.1',
      description:'Easily allow apps to allow linkedin login or registeration'
    }
};
